import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import { styled } from '@mui/material/styles';
const ProductsPages = () => {
  const Item = styled(Paper)(({ theme }) => ({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  }));
  return(    
<>
  <Grid item xs={12} md={2}>
    <Item>xs=12 md=2</Item>
  </Grid>
  <Grid item xs={12} md={8}>
      <h1>ProductsPages</h1>
    <Item>xs=12 md=8</Item>
  </Grid>
  <Grid item xs={12} md={2}>
    <Item>xs=12 md=2</Item>
  </Grid>
   
</>
  ) 
}
export default ProductsPages
