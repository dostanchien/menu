import {useNavigate} from "react-router-dom";
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import { useSelector} from "react-redux";
import { selectorLang} from '../../store/selectors';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import { styled } from '@mui/material/styles';

const NotPage = () => {
  
  const Item = styled(Paper)(({ theme }) => ({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  }));

	const isLang = useSelector(selectorLang); 
	const navigate = useNavigate() /* useNavigate цей хук нам позволяє робити редерект на іншу сторінку за якоюсь умовою або при виконанні якоїсь функції, ще можно зробити переходи по хісторі браузера*/

	const goBack = () => navigate(-1) /*  переходи по хісторі браузера, перехід на попередню сторінку */
	const goToHome = () => navigate('/') /*  редерект на сторінку user */
	return (<>
    <Grid item md={2} sx={{ textAlign: 'center', display: { xs: 'none', md: 'flex' }}}>
    </Grid>
    <Grid item xs={12} md={8} sx={{ textAlign: 'center' }}>
      <Item>
        <Typography variant="h2" component="h1" gutterBottom>
          {!isLang ? '404 помилка!' : '404 ошибка!'}
        </Typography>
      </Item>
      <Typography variant="h6" gutterBottom>
        {!isLang ? 'Сторінку не знайдено' : 'Страница не существует'}
      </Typography>
      <Box my={4}>
        <img
          src="https://diplomus.kiev.ua/images/404.gif"
          alt={!isLang ? '404 помилка' : '404 ошибка'}
          sx={{ maxWidth: '100%', borderRadius: '4px', boxShadow: '0px 2px 10px rgba(0,0,0,0.2)' }}
        />
      </Box>
      <Button variant="contained" color="primary" onClick={goToHome}>
        {!isLang ? 'Перейти на головну' : 'Перейти на главную'}
      </Button>
      <Button variant="outlined" color="secondary" onClick={goBack} sx={{ marginLeft: 8 }}>
        {!isLang ? 'Повернутися назад' : 'Вернуться назад'}
      </Button>
    </Grid>
    <Grid item xs={12} md={2}>
    </Grid>
    </>  );
    };
    
    export default NotPage;