
import React, { useEffect } from 'react';
import { ThemeProvider } from '@mui/material/styles';
import { useDispatch, useSelector } from "react-redux";
import { selectorLang } from './store/selectors.js';
import {actionLang} from "./store/langSlice.js"
import ThemeHeader from './init/ThemeHeader.jsx';
import Grid from '@mui/material/Grid';
import AppRoutes from './components/routes/AppRoutes.jsx';
import styled from '@emotion/styled';
import Header from './components/header/Header.jsx';
import Footer from './components/footer/Footer.jsx';
import { getLangFromUrl } from "./helpers/getLangFromUrl.js";
import './App.css';

const AppContainer = styled.div`
  display: flex;
  flex-direction: column;
  min-height: 100vh;
`;

const StyledGrid = styled(Grid)`
  flex: 1;
`;

function App() {
  // const dispatch = useDispatch();
  // const isLang = useSelector(selectorLang);
  // useEffect(() => {
  // const url = getLangFromUrl(window.location.pathname);
  // const langFromUrl = window.location.pathname.includes('/ru') ? 'ru' : '';

  //  dispatch(actionLang(langFromUrl))

  //   // if (!isLang) {
  //   //   dispatch(url);
  //   // }
  // }, [dispatch]);
  //   //console.log("url2=",url,"lang12=",isLang,"loc2-",useSelector(selectorLang))

  return (
    <AppContainer>
      <ThemeProvider theme={ThemeHeader}>
        <Header />
      </ThemeProvider>
      <StyledGrid container spacing={2} component="main">
        <AppRoutes />
      </StyledGrid>
      <Footer />
    </AppContainer>
  );
}

export default App;
