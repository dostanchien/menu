export const getLangFromUrl = (url) => {
    const langSegment = "/ru/";
    if (url.includes(langSegment)) {
      return "ru";
    } else {
      return "";
    }
  };
  
  