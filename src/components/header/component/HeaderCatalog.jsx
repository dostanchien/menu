import styled from '@emotion/styled';
import PropTypes from 'prop-types';
import { useDispatch} from "react-redux";
import { actionDrawer } from "../../../store/drawerSlice.js";
import HeaderLink from './HeaderLink.jsx';
import Stack from '@mui/material/Stack';

const HeaderCatalog = ({contactConfig,catalogConfig}) => {
  // Проверяем, пусты ли оба массива
  if (!(contactConfig.length > 0) && !(catalogConfig.length > 0)) {
      return null; // Если оба массива пусты, компонент не рендерится
  }
  const dispatch = useDispatch();
  const handelDrawer = () => dispatch(actionDrawer());
  return (
    <StyledNavStack direction="row">
      {/* Рендеринг элементов для 'catalog' пустой не выводим*/}
      {catalogConfig.length > 0 && catalogConfig.map((item, index) => (
        <HeaderLink 
          key={index} 
          icon={item.icon}
          txt={item.text}
          click={handelDrawer}
          aria-controls='menu'
        />
      ))}
      {/* Рендеринг элементов для 'contact' пустой не выводим*/}
      {contactConfig.length > 0 && contactConfig.map((item, index) => (
        <HeaderLink 
          key={index} 
          to={item.url} 
          icon={item.icon}
          txt={item.text}
        />
      ))}
    </StyledNavStack>
  );
};
HeaderCatalog.defaultProps = {
  contactConfig: [],
  catalogConfig: [],
};

HeaderCatalog.propTypes = {
  contactConfig: PropTypes.arrayOf(PropTypes.shape({
    href: PropTypes.string,
    icon: PropTypes.any,
    text: PropTypes.string,
    label: PropTypes.string,
    target:PropTypes.string,
  })),
  catalogConfig: PropTypes.arrayOf(PropTypes.shape({
    href: PropTypes.string,
    icon: PropTypes.any,
    text: PropTypes.string,
    label: PropTypes.string,
    target:PropTypes.string,
  })),
};
export default HeaderCatalog;

const StyledNavStack = styled(Stack)({
    flexShrink: 1,
    marginLeft: '1rem',
  });