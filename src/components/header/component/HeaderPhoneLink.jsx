import PropTypes from 'prop-types';
import Stack from '@mui/material/Stack';
import Typography from '@mui/material/Typography'; // Если вы используете Typography из MUI

const HeaderPhoneLink = ({ item, onClick }) => {
    return (
        <Stack component="a" direction="row" justifyContent="center" rel="nofollow"
            href={item.href}
            onClick={onClick}
            target={item.target || undefined}
            aria-label={item.label}
        >  
            {item.icon}
            {item.text && <Typography variant="subtitle2" component="span">{item.text}</Typography>}
        </Stack>
    );
};

HeaderPhoneLink.defaultProps = {
    onClick: () => {},
    item: {},
};

HeaderPhoneLink.propTypes = {
    onClick: PropTypes.func,
    item: PropTypes.shape({
        href: PropTypes.string,
        icon: PropTypes.node, // Использование PropTypes.node для JSX элементов
        text: PropTypes.string,
        label: PropTypes.string,
        target: PropTypes.string,
    }),
};

export default HeaderPhoneLink;
