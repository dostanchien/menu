import PropTypes from 'prop-types';
import styled from '@emotion/styled';
import Stack from '@mui/material/Stack';
import HeaderLink from './HeaderLink.jsx';

const HeaderShop = ({boxShopItems, onClose})=>{  
    if (!boxShopItems?.length) return null;
    return(    
        <StyledStack direction="row">   
            {boxShopItems.map((item, index) => (
                <HeaderLink
                    key={index}
                    to={item.url}
                    icon={item.icon}
                    txt={item.text}
                    badgeContent={item.badge}
                    click={onClose}
                />
            ))}
        </StyledStack>
    );
}
HeaderShop.defaultProps = {
	onClose: () => {},
  }
  HeaderShop.propTypes = {
    onClose: PropTypes.func,
    boxShopItems:[],
}
HeaderShop.propTypes = {
    boxShopItems: PropTypes.arrayOf(
        PropTypes.shape({
            text: PropTypes.string,
            url: PropTypes.string,
            icon: PropTypes.any,
            badge: PropTypes.number,
        })
    ),
};
export default HeaderShop


const StyledStack = styled(Stack)({
    flexShrink: 1,
    marginLeft: '1rem',
  });