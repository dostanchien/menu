import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';

const DrawerLink = ({ to, onClick, icon, primary, primaryTypographyProps, ...restProps }) => {
  const Component = to ? NavLink : 'div';
  // Формируем дополнительные свойства для NavLink
  const linkProps = to ? { to, end: 'true' } : {};
  return (
    <ListItem 
      component={Component} 
    //  to={to} 
      onClick={onClick} 
      {...linkProps} // Используем распространение свойств для передачи to и end
      {...restProps}
    >
      <ListItemIcon>{icon}</ListItemIcon>
      <ListItemText primary={primary} primaryTypographyProps={primaryTypographyProps} />
    </ListItem>
  );
};

DrawerLink.propTypes = {
  primary: PropTypes.any,
  icon: PropTypes.any,
  to: PropTypes.string,
  onClick: PropTypes.func,
  primaryTypographyProps: PropTypes.object // Додано тип для нового пропу
};

export default DrawerLink;
