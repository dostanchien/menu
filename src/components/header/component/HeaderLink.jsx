import PropTypes from 'prop-types';
import styled from '@emotion/styled';
import { NavLink} from 'react-router-dom';
import Stack from '@mui/material/Stack';
import Badge from '@mui/material/Badge';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';

const HeaderLink = ({ to, txt, icon, badgeContent, ariaLabel, badgeColor, tpVariant, tpComponent, flexDirection, sizeSvg, click, ...restProps }) => {
  const Component = to ? NavLink : 'div';
  // Формируем дополнительные свойства для NavLink
  const linkProps = to ? { to, end: 'true' } : {};
  return (
    <StyledStack 
      component={Component} 
    //  to={to} 
      onClick={click} 
     // end={!!to} // Передаем end только если есть to
     {...linkProps} // Используем распространение свойств для передачи to и end

      {...(ariaLabel ? {'aria-label': ariaLabel} : {})}
  >
      <StyledIconButton 
          tabIndex={-1} 
          aria-haspopup={!to} 
          flexDirection={flexDirection} 
          sizeSvg={sizeSvg} 
          {...restProps}
      >
          {badgeContent && <Badge badgeContent={badgeContent} color={badgeColor}>{icon}</Badge>}
          {!badgeContent && icon}
          <Typography variant={tpVariant} component={tpComponent}>{txt}</Typography>
      </StyledIconButton>
    </StyledStack>
  );
};

HeaderLink.defaultProps = {
    badgeColor:"error",
    tpVariant:"caption",
    tpComponent:'span',
    flexDirection:'column',
    sizeSvg:'2rem',
    click: () => {}
}
HeaderLink.propTypes = {
    to: PropTypes.string,
    ariaLabel: PropTypes.string,
    txt: PropTypes.any,
    idMenu: PropTypes.string,
    badgeColor: PropTypes.string,
    icon: PropTypes.any,
    tpVariant: PropTypes.string,
    badgeContent: PropTypes.number,
    tpComponent: PropTypes.string,
    flexDirection: PropTypes.string,
    sizeSvg: PropTypes.string,
    click: PropTypes.func
}
export default HeaderLink;

const StyledStack = styled(Stack)(({ theme }) => ({
  '& .MuiTypography-root': {  
    color: theme.palette.primary.contrastText,
  },
  '&:hover .MuiIconButton-root, &:hover .MuiTypography-root': {
    color: theme.palette.hover.contrastText,
    'svg': {
      fill: theme.palette.hover.contrastText,
    }
  },
  '&.active .MuiTypography-root, &.active .MuiIconButton-root': {
    color: theme.palette.hover.contrastText,
    'svg': {
      fill: theme.palette.hover.contrastText,
    }
  }
}));

// Создаем вспомогательный компонент для IconButton
const StyledIconButton = styled(({ flexDirection, sizeSvg, ...props }) => <IconButton {...props} />)`
  flex-direction: ${props => props.flexDirection};
  margin: 0 0.5rem 0 0;
  padding: 0;

  svg {
    width: ${props => props.sizeSvg};
    height: ${props => props.sizeSvg};
    fill: ${props => props.theme.palette.primary.contrastText};
  }
`;

