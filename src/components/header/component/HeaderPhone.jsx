import PropTypes from 'prop-types';
import styled from '@emotion/styled';
import Stack from '@mui/material/Stack';
import HeaderPhoneLink from "./HeaderPhoneLink.jsx";

const HeaderPhone = ({ phoneItems, messengerItems, onClick }) => {
  
    // Проверяем, пусты ли оба массива
    if (!(phoneItems.length > 0) && !(messengerItems.length > 0)) {
      return null; // Если оба массива пусты, компонент не рендерится
  }
  return (
    <StackBox>
      <PhoneBlock direction="column">
        {phoneItems.map((item, index) => (
          <HeaderPhoneLink key={index} item={item} onClick={onClick}/>
        ))}
      </PhoneBlock>
      <PhoneBlock direction="row">
        {messengerItems.length > 0 && messengerItems.map((item, index) => (
          <HeaderPhoneLink key={index} item={item} onClick={onClick} />
        ))}
      </PhoneBlock>
    </StackBox>
  );
};

HeaderPhone.defaultProps = {
  onClick: () => {},
  phoneItems: [],
  messengerItems: [],
};

HeaderPhone.propTypes = {
  onClick: PropTypes.func,
  phoneItems: PropTypes.arrayOf(PropTypes.shape({
    href: PropTypes.string,
    icon: PropTypes.any,
    text: PropTypes.string,
    label: PropTypes.string,
    target:PropTypes.string,
  })),
  messengerItems: PropTypes.arrayOf(PropTypes.shape({
    href: PropTypes.string,
    icon: PropTypes.any,
    label: PropTypes.string,
    target:PropTypes.string,
    text: PropTypes.string,
  })),
};

export default HeaderPhone;
const PhoneBlock = styled(Stack)`
  align-items: center;
  flex-shrink: 0;
`;

const StackBox = styled(Stack)(({ theme }) => ({
  flexDirection: 'row',
  flexShrink: 1,
  marginLeft: '0.5rem',
  background: theme.palette.primary.main, // Основной цвет фона из темы

  a: {
    color: theme.palette.primary.contrastText, // Цвет текста ссылок
    transition: 'transform 0.2s ease-in-out, background-color 0.2s ease-in-out',

    '&:hover': {
      transform: 'scale(0.95)',
      backgroundColor: theme.palette.hover.main, // Цвет фона при наведении из темы
      color: theme.palette.hover.contrastText, // Цвет текста при наведении
    },
  },

  svg: {
    transition: 'transform 0.2s ease-in-out',

    '&:hover': {
      transform: 'scale(0.95)',
    },
  },
}));

