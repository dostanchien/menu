import React, { useState, useCallback } from 'react';
import PropTypes from 'prop-types';
import styled from '@emotion/styled';
import Box from '@mui/material/Box';
import Menu from '@mui/material/Menu';
import HeaderShop from "./HeaderShop.jsx";
import HeaderLink from './HeaderLink.jsx';

const DrawShop = (props) => {
    const { boxShop } = props; // Деструктуризация объекта из пропсов
    //Проверяем, что boxShop определен и boxShop.items является массивом
    if (!boxShop || !Array.isArray(boxShop.items) || boxShop.items.length === 0) {
        return null; // Если boxShop не определен, или items не массив, или items пуст
    }
    const [anchorElNav, setAnchorElNav] = useState(null);
    const handleOpenNavMenu = useCallback((event) => {
        setAnchorElNav(event.currentTarget);
    }, []);
    const handleCloseNavMenu = useCallback(() => {
        setAnchorElNav(null);
    }, []);
    // Считаем сумму значений badge
    const badgeSum = boxShop.items.reduce((sum, item) => sum + (item.badge || 0), 0);

    return (
      <StyledBox>
          <HeaderLink 
              click={handleOpenNavMenu} 
              aria-controls="menu-usershop" 
              badgeContent={badgeSum}
              icon={boxShop.icon} 
              txt={boxShop.title}
              ariaLabel={boxShop.ariaLabel}
          />
          <StyledMenu
              id="menu-usershop"
              anchorEl={anchorElNav}
              anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
              keepMounted
              transformOrigin={{ vertical: 'top', horizontal: 'right' }}
              open={Boolean(anchorElNav)}
              onClose={handleCloseNavMenu}
          >
              <HeaderShop boxShopItems={boxShop.items} onClose={handleCloseNavMenu} />
          </StyledMenu>
      </StyledBox>
  );
}
DrawShop.defaultProps = {
    boxShop: {},
};

DrawShop.propTypes = {
    title: PropTypes.string,
    items: PropTypes.arrayOf(
        PropTypes.shape({
            text: PropTypes.string,
            url: PropTypes.string,
            icon: PropTypes.any,
            badge: PropTypes.number,
        })
    ),
    icon: PropTypes.any,
    ariaLabel: PropTypes.string
};
export default DrawShop;

const StyledBox = styled(Box)({
    flexShrink: 1,
    display: 'flex',
  });
  
  const StyledMenu = styled(Menu)(({ theme }) => ({
    '& .MuiPaper-root': {
        backgroundColor: theme.palette.primary.main,
        color: theme.palette.primary.contrastText
    },
}));