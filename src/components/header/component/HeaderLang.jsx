import React, { useEffect } from "react";
import { useNavigate, useLocation } from 'react-router-dom';
import Button from '@mui/material/Button';
import { useDispatch, useSelector } from "react-redux";
import { selectorLang } from "../../../store/selectors";
import { actionLang } from '../../../store/langSlice';
import CheckIcon from '@mui/icons-material/Check';
import Stack from '@mui/material/Stack';
import styled from '@emotion/styled';
import RusIcon from "../../../icons/rus.svg?react";
import UaIcon from "../../../icons/ua.svg?react";

const HeaderLang = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const location = useLocation();
  const isLang = useSelector(selectorLang);

  const toggleLang = () => {
    dispatch(actionLang());
    // Отримуємо поточний шлях без мовного префіксу
    const path = location.pathname.replace(/^\/ru/, '');
    // Визначаємо новий URL
    const newUrl = !isLang ? `/ru${path}` : path;
    navigate(newUrl);
  };

  return (
    <StyledStack direction="column" justifyContent="start">
      <StyledCursor cursor="pointer">
        <LangButton
          onClick={toggleLang}
          startIcon={<UaIcon />}
          endIcon={!isLang && <CheckIconStyled />}
          title="Українська"
          disabled={!isLang?true:false}
        />
      </StyledCursor>
      <StyledCursor cursor="pointer">
        <LangButton
          onClick={toggleLang}
          startIcon={<RusIcon />}
          endIcon={isLang && <CheckIconStyled />}
          title="Русский"
          disabled={isLang?true:false}
        />
      </StyledCursor>
    </StyledStack>
  );
};

export default HeaderLang;

const LangButton = styled(Button)({
  minWidth: '30px',
  padding: 0,
  justifyContent: "start",
  margin: '0.1rem 0 0.1rem 0',
  '&:hover svg': { transform: 'scale(0.8)' },
  '& .MuiButton-startIcon, & .MuiButton-endIcon': { margin: 0 },
});

const CheckIconStyled = styled(CheckIcon)(({ theme}) => ({
  lineHeight: 1,
  transform: 'scale(0.7)',
  background: theme.palette.checked.main,
  fill: theme.palette.checked.contrastText,
  borderRadius: '50%',
}));

const StyledCursor = styled(Stack)`
  cursor: ${props => props.cursor};
`;

const StyledStack = styled(Stack)`
  flex-shrink: 1;
  margin-left: 0.5rem;
`;
