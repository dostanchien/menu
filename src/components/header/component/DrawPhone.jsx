import React, { useState, useCallback } from 'react';
import PropTypes from 'prop-types';
import styled from '@emotion/styled';
import Box from '@mui/material/Box';
import Menu from '@mui/material/Menu';
import HeaderPhone from "./HeaderPhone.jsx";
import HeaderLink from './HeaderLink.jsx';

const DrawPhone = ({boxHeaderPhone}) => {
  
    // Проверка на наличие boxHeaderPhone и хотя бы одного непустого массива внутри него
    if (!boxHeaderPhone || 
        (!boxHeaderPhone.phoneHeaderPhone?.length && !boxHeaderPhone.messengerHeaderPhone?.length)) {
        return null; // Если объект пуст или оба массива пусты, компонент не рендерится
    }
    const [anchorElNav, setAnchorElNav] = useState(null);
    const handleOpenNavMenu = useCallback((event) => {
        setAnchorElNav(event.currentTarget);
    }, []);
    const handleCloseNavMenu = useCallback(() => {
        setAnchorElNav(null);
    }, []);

    return (    
        <StyledBox>
            <HeaderLink 
                click={handleOpenNavMenu} 
                aria-controls="menu-phone" 
                icon={boxHeaderPhone.icon}
                txt={boxHeaderPhone.title}
                aria-label={boxHeaderPhone.ariaLabel}
            />
            <Menu
                id="menu-phone"
                anchorEl={anchorElNav}
                anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
                keepMounted
                transformOrigin={{ vertical: 'top', horizontal: 'right' }}
                open={Boolean(anchorElNav)}
                onClick={handleCloseNavMenu}
            >
                <HeaderPhone phoneItems={boxHeaderPhone.phoneHeaderPhone} messengerItems={boxHeaderPhone.messengerHeaderPhone} onClick={handleCloseNavMenu} />
            </Menu>
        </StyledBox>
    );
};
DrawPhone.defaultProps = {
    boxHeaderPhone: {},
};

DrawPhone.propTypes = {
    title: PropTypes.string,
    icon: PropTypes.any,
    ariaLabel: PropTypes.string,
    phoneHeaderPhone: PropTypes.arrayOf(
        PropTypes.shape({
            href: PropTypes.string,
            icon: PropTypes.any,
            text: PropTypes.string,
            label: PropTypes.string
        })
    ),
    messengerHeaderPhone: PropTypes.arrayOf(
        PropTypes.shape({
            href: PropTypes.string,
            icon: PropTypes.any,
            label: PropTypes.string,
            target: PropTypes.string
        })
    )
};
export default DrawPhone;

const StyledBox = styled(Box)({
    flexShrink: 1,
    display: 'flex',
  });
