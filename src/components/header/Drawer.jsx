import React, { useState } from 'react';
import { useLocation } from 'react-router-dom';
import { useTheme } from '@mui/material/styles';
import { useSelector, useDispatch } from "react-redux";
import { selectorDrawer, selectorLang,selectorMenuRu, selectorMenuUa} from '../../store/selectors';
import { actionDrawer } from "../../store/drawerSlice";
import Drawer from '@mui/material/Drawer';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import ListItemIcon from '@mui/material/ListItemIcon';
import Collapse from '@mui/material/Collapse';
import FolderOpenIcon from '@mui/icons-material/FolderOpen';
import FolderIcon from '@mui/icons-material/Folder';
import styled from '@emotion/styled';
import Box from '@mui/material/Box';
import Divider from '@mui/material/Divider';
import DrawerLink from './component/DrawerLink.jsx';
import {getIconComponent}  from '../../init/IconHelpers.jsx';
import getConfigMenu from '../../init/getConfigMenu.jsx';


const DrawerMenu = () => {
 
  const dispatch = useDispatch();
  const isDrawer = useSelector(selectorDrawer);
  const isLang = useSelector(selectorLang); 
  const menuData = isLang ?  useSelector(selectorMenuRu) : useSelector(selectorMenuUa); 
  const [open, setOpen] = useState({});
  const handleClick = (category) => {
    setOpen(prevOpen => ({
      ...prevOpen,
      [category]: !prevOpen[category]
    }));
  };
  
 const ariaLabelCategory = !isLang ? 
  {open:"Развернуть категорию",close:"Свернуть категорию"}
 :
  {open:"Розгорнути категорію",close:"Згорнути категорію"};
  
  const headerCatalog = getConfigMenu("contactHeaderCatalog", isLang);
  const location = useLocation();
  const isCategoryActive = (categoryItems) => {
    return categoryItems.some(item => location.pathname === item.url);
  };
  return (
    <Drawer open={isDrawer} onClose={() => dispatch(actionDrawer())}>
      <Box component="nav" role="navigation">
        <List sx={{m:0, p:0}}>
          <StyledDrawerLink key="logotype"
            to={getConfigMenu("logotype", isLang).url}
            onClick={() => dispatch(actionDrawer())}
            icon={getConfigMenu("logotype", isLang).icon}
            primary={getConfigMenu("logotype", isLang).text}
            primaryTypographyProps={{ variant: 'h6' }} 
          />
        </List>
        
              <List>
          {menuData.map((item, index) => (
            item.items && item.items.length > 0 ? (
              <React.Fragment key={index}>
                <ListItem   divider button 
                            onClick={() => handleClick(item.category)}
                            aria-expanded={open[item.category] ? "true" : "false"}
                            aria-label={open[item.category] ? ariaLabelCategory.open : ariaLabelCategory.close}
                >
                  <ListItemIcon sx={(theme) => ({color: isCategoryActive(item.items) ? theme.palette.primary.main : 'inherit' })}>
                    {open[item.category] ? getIconComponent(item.iconOpen) : getIconComponent(item.icon)}</ListItemIcon>
                  
                  <ListItemText primary={<CategoryText hasSubItems={true}>{item.category}</CategoryText>} />
                </ListItem>
                <Collapse in={open[item.category] || false} timeout="auto" unmountOnExit>
                  <List component="div" disablePadding>
                    {item.items.map((subItem, subIndex) => (
                    <DrawerLink key={subIndex}
                                    to={subItem.url}
                                    onClick={() => dispatch(actionDrawer())}
                                    sx={{ paddingLeft: '40px' }}
                                    icon={getIconComponent(subItem.icon)}
                                    primary={subItem.name}/>
                    ))}
                  </List><Divider/>
                </Collapse>
              </React.Fragment>
            ) : (
              <DrawerLink key={index}
                              to={item.url}
                              onClick={() => dispatch(actionDrawer())}
                              icon={getIconComponent(item.icon)}
                              primary={item.category} />
            )
          ))}
        </List>
        <Divider />
        <List>
            {headerCatalog.map((item, index) => (
                <DrawerLink
                    key={index}
                    to={item.url}
                    onClick={() => dispatch(actionDrawer())}
                    icon={item.icon}
                    primary={item.text}
                />
            ))}
        </List>
      </Box>
    </Drawer>
  );
};

export default DrawerMenu;

const CategoryText = styled('span')(({ theme, hasSubItems }) => ({
  fontWeight: hasSubItems ? 'bold' : 'normal',
}));

const StyledDrawerLink  = styled(DrawerLink)(({ theme }) => ({
  backgroundColor: theme.palette.primary.main,
  color: theme.palette.primary.contrastText,
  padding:'0.4625rem 0',
  margin:0,
  justifyContent: 'flex-start',
  '&:hover': {
    color: theme.palette.hover.contrastText,
  },
  '&.active': {
    color: '#ffffff !important',
  }
}));

