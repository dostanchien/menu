import React, {useEffect } from 'react';
import { useDispatch, useSelector } from "react-redux";
import { selectorLang } from '../../store/selectors.js';
import AppBar from '@mui/material/AppBar';
import styled from '@emotion/styled';
import Toolbar from '@mui/material/Toolbar';
import MenuIcon from '@mui/icons-material/Menu';
import {actionDrawer } from "../../store/drawerSlice.js";
import {actionFetchMenu, } from '../../store/langSlice';
import HeaderLang from "./component/HeaderLang.jsx";
import HeaderPhone from "./component/HeaderPhone.jsx";
import HeaderCatalog from './component/HeaderCatalog.jsx';
import HeaderShop from './component/HeaderShop.jsx';
import useMediaQuery from '@mui/material/useMediaQuery';
import DrawerMenu from './Drawer.jsx';
import DrawShop from './component/DrawShop.jsx';
import Stack from '@mui/material/Stack';
import DrawPhone from './component/DrawPhone.jsx';
import HeaderLink from './component/HeaderLink.jsx';
import getConfigMenu from '../../init/getConfigMenu.jsx';

const Header = ()=>{
  const dispatch = useDispatch();
  useEffect(() => {  
          dispatch(actionFetchMenu());
  }, [dispatch]);
  const handelDrawer = (is) => dispatch(actionDrawer(is));
  const isMinWidth650 = useMediaQuery('(min-width:650px)');
  const isMaxWidth550 = useMediaQuery('(max-width:550px)');
  const isMinWidth550 = useMediaQuery('(min-width:551px)');
  const isMinWidth980 = useMediaQuery('(min-width:981px)');
  const isMaxWidth980 = useMediaQuery('(max-width:980px)');
  const isMinWidth860 = useMediaQuery('(min-width:861px)');
  const isMaxWidth860 = useMediaQuery('(max-width:860px)');

  const isLang = useSelector(selectorLang);
  return(    
    <AppBar position="static">
      <StyledToolbar component="nav">
        {(isMinWidth650 || isMaxWidth550) && <HeaderLink to={getConfigMenu("logotype", isLang).url} tpVariant="h6" icon={getConfigMenu("logotype", isLang).icon} flexDirection="row" txt={getConfigMenu("logotype", isLang).text}/>}
        {isMinWidth550 && <HeaderCatalog contactConfig={getConfigMenu("contactHeaderCatalog", isLang)} catalogConfig={getConfigMenu("catalogHeaderCatalog", isLang)}/>}
        <Stack sx={{ flexGrow: 1 }}></Stack>
        {isMinWidth980 && <HeaderShop boxShopItems={getConfigMenu("boxShop.items", isLang)}/>}
        {isMaxWidth980 && <DrawShop boxShop={getConfigMenu("boxShop", isLang)}/>}
        {isMinWidth860 && <HeaderPhone phoneItems={getConfigMenu("boxHeaderPhone.phoneHeaderPhone", isLang)} messengerItems={getConfigMenu("boxHeaderPhone.messengerHeaderPhone", isLang)}/>}
        {isMaxWidth860 && <DrawPhone boxHeaderPhone={getConfigMenu("boxHeaderPhone", isLang)}/>}
        <HeaderLang/>
        {isMaxWidth550 && <HeaderLink click={() => handelDrawer()} aria-controls='menu' icon={<MenuIcon/>} flexDirection="row" txt="Меню"/>} 
        <DrawerMenu id="menu"/> 
      </StyledToolbar>
    </AppBar>
  )
}
export default Header

const StyledToolbar = styled(Toolbar)`
  @media (orientation: landscape) {
    min-height: 4rem;
  }
  & .MuiTypography-caption {
    font-size: 0.8rem;
    line-height: 1;
  }
`;