import React from 'react';
import {Routes, Route} from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { selectorLang } from "../../store/selectors";
import HomePage from '../../pages/HomePage/HomePage.jsx';
import DeliveryPage from '../../pages/DeliveryPage/DeliveryPage.jsx'
import ContactPage from '../../pages/ContactPage/ContactPage.jsx';
import BalancePage from '../../pages/BalancePage/BalancePage.jsx';
import FavoritePage from '../../pages/FavoritePage/FavoritePage.jsx';
import OrderPage from '../../pages/OrderPage/OrderPage.jsx';
import NotPage from '../../pages/NotPage/NotPage.jsx'
import PolicyPage  from '../../components/footer/PolicyPage.jsx';
import DynamicPage from '../routes/DynamicPage/DynamicPage.jsx';
const AppRoutes = ()  =>{
	const isLang = useSelector(selectorLang);
	const dispatch = useDispatch();
 
	return (  
		<>
<Routes>
      	<Route path={isLang} element={<HomePage/>}/>;
      	<Route path={isLang+"/delivery"} element={<DeliveryPage/>}/>;
      	<Route path={isLang+"/contacts"} element={<ContactPage/>}/>;
      	<Route path={isLang+"/balance"} element={<BalancePage/>}/>;
      	<Route path={isLang+"/favorite"} element={<FavoritePage/>}/>;
      	<Route path={isLang+"/order"} element={<OrderPage/>}/>;
      	<Route path={isLang+"/policy"} element={<PolicyPage/>}/>;
		<Route path={isLang+"/:part1/:part2?/:part3?/:part4?"} element={<DynamicPage />} />;

		<Route path={isLang+"/*"} element={<NotPage/>}/>;
		
</Routes>
{/* {console.log("path={isLang+"/,isLang+"/delivery")} */}
	</>
	)
}
export default AppRoutes

