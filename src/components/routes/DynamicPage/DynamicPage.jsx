
import React from 'react';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import { styled } from '@mui/material/styles';
import { useParams } from 'react-router-dom';
import CatalogPages from '../../../pages/CatalogPages/CatalogPages.jsx';
import SubCatalogPages from '../../../pages/SubCatalogPages/SubCatalogPages.jsx';
import BrandPages from '../../../pages/BrandPages/BrandPages.jsx';
import ProductsPages from '../../../pages/ProductsPages/ProductsPages.jsx';

function DynamicPage() {
  const { part1, part2, part3, part4 } = useParams();

  // Примерная логика определения, какую страницу отобразить
  if (part1 && !part2 && !part3 && !part4) {
    return <CatalogPages catalogName={part1} />;
  } else if (part1 && part2 && !part3 && !part4) {
    return <SubCatalogPages subCatalogName={part2} />;
  } else if (part1 && part2 && part3 && !part4) {
    return <BrandPages brandName={part3} />;
  } else if (part1 && part2 && part3 && part4) {
    return <ProductsPages productId={part4} />;
  } else {
    // Если не подходит ни под один из случаев
    return <div>Страница не найдена</div>;
  }
}

export default DynamicPage;
