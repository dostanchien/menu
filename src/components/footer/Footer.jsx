import React from 'react';
import { useSelector } from "react-redux";
import {selectorLang} from '../../store/selectors';
import styled from '@emotion/styled';
import { NavLink } from 'react-router-dom';
const Footer = () => {
  const isLang = useSelector(selectorLang); 
  const policyPath = !isLang ? '/policy' : '/ru/policy';
  const policyText = !isLang ? 'Політика конфіденційності' : 'Политика конфиденциальности';

  // Функція для Bigmir, яка вставляється як inline script
  const bigmirScript = () => {
    // JavaScript код для Bigmir
  };

  // Функція для Google Analytics, яка вставляється як inline script
  const googleAnalyticsScript = () => {
    // Для Google Analytics, и офіційний пакет для React 
  };

    return (
      <StyledFooter>
      <StyledNavLink to={policyPath}>{policyText}</StyledNavLink>
      <br />
      Copyright © 2011 - {new Date().getFullYear()} Diplomus
    </StyledFooter>
    );
  };
  
  export default Footer;
  const StyledFooter = styled.footer`
  background-color: #000;
  color: white;
  text-align: center;
  margin-top: 4rem;
  padding: 1rem 0;
`;

const StyledNavLink = styled(NavLink)`
  color: white;
  text-decoration: none;

  &:hover {
    text-decoration: underline;
  }
`;

