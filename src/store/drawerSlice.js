import PropTypes from 'prop-types'
import {createSlice} from "@reduxjs/toolkit";

const drawerSlise = createSlice({
    name : "drawer",
    initialState:{
        isDrawer: false
    },
    reducers:{
        actionDrawer: (state)  =>  {
            state.isDrawer = !state.isDrawer
        }      
    }
})
drawerSlise.propTypes = {
    isDrawer: PropTypes.bool
}
export const {actionDrawer} = drawerSlise.actions
export default drawerSlise.reducer