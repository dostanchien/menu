import {createSlice} from "@reduxjs/toolkit";

const favoriteSlise = createSlice({
    name : "favorite",
    initialState:{
		favorites:{},
		currentPost: {},
        isModal: false
    },
    reducers:{
        actionFavoriteCooki: (state) => {state.favorites = JSON.parse(localStorage.getItem("Favorite"))||{}},
        actionFavorite:(state,{payload})=>{
            if( !state.favorites[payload.article]) {
                state.favorites  = { ...state.favorites , [payload.article] : 1}                      
            } else {
                delete state.favorites[payload.article]
            }
            localStorage.setItem("Favorite", JSON.stringify(state.favorites))
        },
		actionCurrentPost: (state,{payload}) => {
			state.currentPost = {...payload}
		},
        actionModal: (state) => {
            state.isModal = !state.isModal
        }      
    }
})

export const {actionFavorite,actionModal,actionFavoriteCooki,actionCurrentPost} = favoriteSlise.actions
export default favoriteSlise.reducer