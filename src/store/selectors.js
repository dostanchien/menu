export const selectorDrawer = (state) => state.drawer.isDrawer;
export const selectorLang = (state) => state.lang.isLang;
export const selectorLoading = (state) => state.lang.isLoading;
export const selectorMenuUa = (state) => state.lang.uaDataMenu;
export const selectorMenuRu = (state) => state.lang.ruDataMenu;