import {createSlice} from "@reduxjs/toolkit";
import { sendRequest } from "../helpers/sendRequest.js"

const orderSlise = createSlice({
    name : "order",
    initialState:{
        count:{},
        currentPost: {},
        isLoading: true,
        isModal: false
    },
    reducers:{
        actionDeleteCooki: (state) => {
                        state.count = {}
                        localStorage.setItem("OrderId", JSON.stringify(state.count))
        },
        actionOrderCooki: (state) => {
                        state.count = JSON.parse(localStorage.getItem("OrderId"))||{}
        },     
        actionOrder:    (state,{payload})=>{
                        if( !state.count[payload.article]) {
                            state.count  = { ...state.count , [payload.article] : 1}                      
                        } else {
                            delete state.count[payload.article]
                        }
                        localStorage.setItem("OrderId", JSON.stringify(state.count))
        },
		actionCurrentPost: (state,{payload}) => {
			state.currentPost = {...payload}
		},
        actionOrderIsModal: (state) => {
                        state.isModal = !state.isModal
        },  
        actionPlus: (state,{payload}) => {          
            state.count  = { ...state.count , [payload.article] : state.count[payload.article]+1}
            localStorage.setItem("OrderId", JSON.stringify(state.count))
        },  
        actionMinus: (state,{payload}) => { 
            if (state.count[payload.article] >1){       
                state.count  = { ...state.count , [payload.article] : state.count[payload.article]-1}
                localStorage.setItem("OrderId", JSON.stringify(state.count))
            }
        },
        actionLoading: (state,{payload}) => {
            state.isLoading = payload
        }
        
    }
})

export const {actionOrder,actionOrderIsModal,actionDeleteCooki,actionPlus,actionMinus,actionOrderCooki,actionOrderReload,actionLoading,actionCurrentPost} = orderSlise.actions
export default orderSlise.reducer