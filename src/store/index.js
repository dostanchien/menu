import {configureStore} from "@reduxjs/toolkit";
import drawerReducer from "./drawerSlice";
import langReducer from "./langSlice";
import favoriteReducer from "./favoriteSlice";
import orderReducer from "./orderSlice";

export default configureStore({
    reducer: {
        drawer:drawerReducer,
        lang:langReducer,
        //dataMenu:menuReducer,
        //product: productsReducer,
        //favorite: favoriteReducer,
        //order: orderReducer,
    },
})