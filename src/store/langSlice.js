import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { sendRequest } from "../helpers/sendRequest.js"

// // Асинхронный thunk для загрузки языковой настройки из localStorage
// export const fetchLangSetting = createAsyncThunk(
//     'lang/fetchLangSetting',
//     async () => {
//         const langValue = localStorage.getItem("Lang");
//         return langValue !== null ? JSON.parse(langValue) : '';
//     }
// );

// // Асинхронный thunk для загрузки языковой настройки из url
export const fetchLangSetting = createAsyncThunk(
    'lang/fetchLangSetting',
    async () => {
        const url = window.location.pathname;
        const isRussian = url.startsWith('/ru/') || url === '/ru' || url.startsWith('/ru?');
        return isRussian ? 'ru' : '';
    }
);



const langSlice = createSlice({
    name: "lang",
    initialState: {
        uaDataMenu: [],
        ruDataMenu: [],
        isLoading: true,
        isLang: '',
    },
    reducers: {
        actionAddToUa: (state, { payload }) => {
            state.uaDataMenu = [...payload];
        },
        actionAddToRu: (state, { payload }) => {
            state.ruDataMenu = [...payload];
        },
        actionLoading: (state, { payload }) => {
            state.isLoading = payload;
        },
        actionLang: (state, action) => {
            // Перевіряємо, чи було передано якусь мову як payload
            if (action.payload) {
              state.isLang = action.payload;
            } else {
              // Якщо payload не передано, просто перемикаємо мову
              state.isLang = state.isLang === '' ? 'ru' : '';
            }
             localStorage.setItem("Lang", JSON.stringify(state.isLang));
        }      
    },
    extraReducers: (builder) => {
        builder.addCase(fetchLangSetting.fulfilled, (state, action) => {
            if (state.isLang !== action.payload) {
                state.isLang = action.payload;
            }
        });
    }
      
});

export const {actionAddToUa, actionAddToRu, actionLoading, actionLang} = langSlice.actions;

export const actionFetchMenu = () => (dispatch) => {
    dispatch(actionLoading(true));
    return sendRequest(window.location.origin + '/menu.json')
        .then((data) => { 
            dispatch(actionAddToUa(data.ua));
            dispatch(actionAddToRu(data.ru));
            dispatch(actionLoading(false));
        });
};

export default langSlice.reducer;
