import { createTheme } from '@mui/material/styles';

const ThemeHeader = createTheme({
  palette: {
    primary: {
      main: '#1976d2', // Стандартный синий цвет фона AppBar
      contrastText: '#ffffff', // Белый цвет текста
    },
    hover: {
      main: '#1871ca',//фон
      contrastText: 'aqua',//текст
    },
    checked: {
      main: '#f44336', //фон переключение языка иконка ИконЧекед
      contrastText: '#ffffff', // Белый цвет текста переключение языка иконка ИконЧекед

    },
    drawer: {
      hoverBackground: '#e2e4e5',
      hoverText: '#6c757d',
      activeBackground: '#e2e4e5',
      activeText: '#6c757d',
      activeSvg: '#1976d2',
    },
  },
  components: {
    MuiAppBar: {
      styleOverrides: {
        root: (theme) => ({
          color: (theme) => theme.palette.primary.contrastText,
          '&:hover': {
            backgroundColor: (theme) => theme.palette.hover.contrastText,
            color: (theme) => theme.palette.hover.contrastText,
          },
        }),
      },
    },
    MuiDrawer: {
      styleOverrides: {
        paper: {
          width: '22rem',
          boxSizing: 'border-box',
          '& .MuiListItem-root': {
            '& svg': {
              width:'2rem', height:'2rem',
            },
            '&:hover': {
              backgroundColor: (theme) => theme.palette.primary.main,
              fontStyle: 'italic',
              color: (theme) => theme.palette.drawer.hoverText,
              '& svg': {
                fill: (theme) => theme.palette.primary.main,
              },
            },
            '&.active': {
              color: '#1976d2',
              fontStyle: 'italic',
              cursor:'not-allowed',
              'svg': {
                fill: '#1976d2',
              },
            },
          },
        },
      },
    },
    
  },
});
export default ThemeHeader
