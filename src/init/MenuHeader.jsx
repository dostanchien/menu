
import FolderIcon from '@mui/icons-material/Folder';
import HeatingIcon from '@mui/icons-material/AcUnit'; // Пример иконки для категории "Обогреватели"
import BoilerIcon from '@mui/icons-material/Opacity'; // Пример иконки для категории "Котлы"
import ThermostatIcon from '@mui/icons-material/Thermostat'; // Пример иконки для "Терморегуляторы"
import ElectricIcon from '@mui/icons-material/ElectricBolt'; // Пример иконки для "Электрические"
import SmokeFreeIcon from '@mui/icons-material/SmokeFree'; // Пример иконки для "Дымоходы"

const iconMap = {
    'HeatingIcon': HeatingIcon,
    'ElectricIcon': ElectricIcon,
    'FolderIcon': FolderIcon,
    'BoilerIcon': BoilerIcon,
    'ThermostatIcon': ThermostatIcon,
    'SmokeFreeIcon': SmokeFreeIcon
  };
export const  getIconComponent = (iconName) => {
    const Icon = iconMap[iconName];
    return Icon ? <Icon /> : null;
  };

