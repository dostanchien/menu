import React from 'react';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import FavoriteIcon from '@mui/icons-material/Favorite';
import BalanceIcon from '@mui/icons-material/AccountBalance';
import MenuIcon from '@mui/icons-material/Menu';
import LocalShippingIcon from '@mui/icons-material/LocalShipping';
import ContactMailIcon from '@mui/icons-material/ContactMail';
import ContactPhoneIcon from '@mui/icons-material/ContactPhone';
import Viber from  "../icons/viber.svg?react";
import Telegram from "../icons/telegram.svg?react";
import Vodafone from "../icons/vodafone.svg?react";
import LifeCell from "../icons/lifecell.svg?react";
import WhatsApp from  "../icons/whatsapp.svg?react";
import UserShopIcon from '../icons/usershop.svg?react';
import Roza from "../icons/roza.svg?react";

const getConfigMenu = (prop,isLang)=>{

    const allconfig ={
        logotype: { url: "/"+isLang, text: "Diplomus", icon: <Roza/> },
        catalogHeaderCatalog: [{ url: isLang+"/catalog", icon: <MenuIcon/> }],
        contactHeaderCatalog: [
            { url: isLang+"/delivery", icon: <LocalShippingIcon/> },
            { url: isLang+"/contacts", icon: <ContactMailIcon/> }
        ],
        boxShop:{
                items: [
                    { url: isLang+"/balance", icon: <ShoppingCartIcon/>, badge:1 },
                    { url: isLang+"/favorite", icon: <FavoriteIcon/>, badge:1 },
                    { url: isLang+"/order", icon: <BalanceIcon/>, badge:1 }
                ],
                icon: <UserShopIcon />,
        },
        boxHeaderPhone:{
                title:"Телефон",
                icon:<ContactPhoneIcon />,
                phoneHeaderPhone: [
                    { href: "tel:+380934915045", icon: <LifeCell />, text: "+380 (93) 491 50 45", label:"LifeCell" },
                    { href: "tel:+380668117899", icon: <Vodafone />, text: "+380 (50) 811 78 99", label:"Vodafone"}
                ],
                messengerHeaderPhone: [
                    { href: "https://telegram.im/diplomuskievua",   icon: <Telegram />, label:"Telegram", target: '_blank'  },
                    { href: "viber://chat?number=380668117899",     icon: <Viber />,    label:"Viber" },
                    { href: "whatsapp://send?phone=+380668117899",  icon: <WhatsApp />, label:"WhatsApp"}
                ],
        },
    } 
const langConfig = {
    ua: {
      catalogHeaderCatalog: [{ text: "Каталог"}],
      contactHeaderCatalog: [
        { text: "Доставка"},
        { text: "Контакти"}
      ],
      boxShop:{
        title: "Обране",
        items: [
            { text: "Порівняти"},
            { text: "Улюблене"},
            { text: "Кошик"}
        ],
        ariaLabel:"Розкрити меню вибраного. Містить посилання на кошик товарів, список товарів, що сподобалися, і товари для порівняння."
        },
      boxHeaderPhone: { 
        title: "Телефон", 
        ariaLabel: "Розгорнути меню контактів", 
      }
    },
    ru: {
      catalogHeaderCatalog: [{ text: "Каталог"}],
      contactHeaderCatalog: [
        { text: "Доставка"},
        { text: "Контакты"}
      ],
      boxShop:{
        title: "Выбранное", 
        items: [
            { text: "Сравнить"},
            { text: "Избранное"},
            { text: "Корзина"}
        ],
        ariaLabel:"Раскрыть меню выбранного. Содержит ссылки на корзину товаров, список понравившихся товаров и товары для сравнения."
},
      boxHeaderPhone: { 
        title: "Телефон", 
        ariaLabel: "Розгорнути меню контактів", 
      }
    }
  };
  function deepMerge(target, source) {
    if (React.isValidElement(source)) {
        return source;
    }

    if (typeof source === 'object' && source !== null) {
        if (Array.isArray(source)) {
            return source.map((item, index) => 
                index < (target || []).length ? deepMerge(target[index], item) : item
            );
        } else {
            const result = { ...target };
            Object.keys(source).forEach(key => {
                result[key] = key in target ? deepMerge(target[key], source[key]) : source[key];
            });
            return result;
        }
    }
    return source;
}

function mergeConfigs(allConfig, langConfig) {
    return deepMerge(allConfig, langConfig);
}

  
  
  
 // Выбор конфигурации на основе текущего языка
 const currentConfig = isLang ? mergeConfigs(allconfig, langConfig.ru) : mergeConfigs(allconfig, langConfig.ua);

 // Путь к свойству разбивается на части
 const propsPath = prop.split(".");

 // Доступ к свойству через цепочку вызовов
 let result = currentConfig;
 for (const p of propsPath) {
     result = result[p];
     if (!result) break; // Если какое-то свойство не найдено, прерываем цикл
 }

 return result||[];
};
export default getConfigMenu;
