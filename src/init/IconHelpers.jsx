
import FolderOpenIcon from '@mui/icons-material/FolderOpen';
import FolderIcon from '@mui/icons-material/Folder';
import CeramicInfra from '../icons/catalog/ceramicInfra.svg?react'; // "ceramic infra"
import ElectroRadiator from '../icons/catalog/electricRadiator.svg?react'; // "electro radiator"
import SolidFuelBoiler from '../icons/catalog/solidFuelBoiler.svg?react'; // "Котлы твердотопливные"
import ElectricBoiler from '../icons/catalog/electricBoiler.svg?react'; // "Котлы электрические"
import ThermoRegulator from '../icons/catalog/thermoRegulator.svg?react'; // "Терморегуляторы"
import Stabilizer from '../icons/catalog/stabilizer.svg?react'; // "Стабилизаторы"
import Chimney from '../icons/catalog/chimney.svg?react'; // "Дымоходы"
import Recirculator from '../icons/catalog/recirculator.svg?react'; // "Дымоходы"

const iconMap = {
    'ElectricBoiler': ElectricBoiler,
    'CeramicInfra': CeramicInfra,
    'FolderIcon': FolderIcon,
    'FolderOpenIcon':FolderOpenIcon,
    'ThermoRegulator': ThermoRegulator,
    'Stabilizer': Stabilizer,
    'SolidFuelBoiler': SolidFuelBoiler,
    'ElectroRadiator':ElectroRadiator,
    'Chimney':Chimney,
    'Recirculator':Recirculator,


  };
export const  getIconComponent = (iconName) => {
    const Icon = iconMap[iconName];
    return Icon ? <Icon /> : null;
  };

