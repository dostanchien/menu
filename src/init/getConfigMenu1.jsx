import React from 'react';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import FavoriteIcon from '@mui/icons-material/Favorite';
import BalanceIcon from '@mui/icons-material/AccountBalance';
import MenuIcon from '@mui/icons-material/Menu';
import LocalShippingIcon from '@mui/icons-material/LocalShipping';
import ContactMailIcon from '@mui/icons-material/ContactMail';
import ContactPhoneIcon from '@mui/icons-material/ContactPhone';
import Viber from  "../icons/viber.svg?react";
import Telegram from "../icons/telegram.svg?react";
import Vodafone from "../icons/vodafone.svg?react";
import LifeCell from "../icons/lifecell.svg?react";
import WhatsApp from  "../icons/whatsapp.svg?react";
import UserShopIcon from '../icons/usershop.svg?react';
import Roza from "../icons/roza.svg?react";

const getConfigMenu = (prop,isLang)=>{
    const config =  !isLang ?
        {ru:{
            logotype:{text:"Diplomus",icon:<Roza/>},
            catalogHeaderCatalog: [{ text: "Каталог", url: "/catalog", icon: <MenuIcon/> }],
            contactHeaderCatalog: [
                { text: "Доставка", url: "/delivery", icon: <LocalShippingIcon/> },
                { text: "Контакты", url: "/contacts", icon: <ContactMailIcon/> }
            ],
            boxShop:{
                    title: "Выбранное", 
                    items: [
                        { text: "Сравнить", url: "/balance", icon: <ShoppingCartIcon/>, badge:1 },
                        { text: "Избранное", url: "/favorite", icon: <FavoriteIcon/>, badge:1 },
                        { text: "Корзина", url: "/order", icon: <BalanceIcon/>, badge:1 }
                    ],
                    icon: <UserShopIcon />,
                    ariaLabel:"Раскрыть меню выбранного. Содержит ссылки на корзину товаров, список понравившихся товаров и товары для сравнения."
            },
            boxHeaderPhone:{
                    title:"Телефон",
                    icon:<ContactPhoneIcon />,
                    ariaLabel:"Розгорнути меню контактів",
                    phoneHeaderPhone: [
                        { href: "tel:+380934915045", icon: <LifeCell />, text: "+380 (93) 491 50 45", label:"LifeCell" },
                        { href: "tel:+380668117899", icon: <Vodafone />, text: "+380 (50) 811 78 99", label:"Vodafone"}
                    ],
                    messengerHeaderPhone: [
                        { href: "https://telegram.im/diplomuskievua",   icon: <Telegram />, label:"Telegram", target: '_blank'  },
                        { href: "viber://chat?number=380668117899",     icon: <Viber />,    label:"Viber" },
                        { href: "whatsapp://send?phone=+380668117899",  icon: <WhatsApp />, label:"WhatsApp"}
                    ],
            },
            } 
        } 
    :
        {ua:{
            logotype:{text:"Diplomus",icon:<Roza/>},
            catalogHeaderCatalog: [{ text: "Каталог", url: "/catalog", icon: <MenuIcon/> }],
            contactHeaderCatalog: [
                { text: "Доставка", url: "/delivery", icon: <LocalShippingIcon/> },
                { text: "Контакти", url: "/contacts", icon: <ContactMailIcon/> }
            ],
            boxShop:{
                title: "Обране",
                items: [
                    { text: "Порівняти", url: "/balance", icon: <ShoppingCartIcon/>, badge:1 },
                    { text: "Улюблене", url: "/favorite", icon: <FavoriteIcon/>, badge:1 },
                    { text: "Кошик", url: "/order", icon: <BalanceIcon/>, badge:1 }
                ],
                icon: <UserShopIcon/>,
                ariaLabel:"Розкрити меню вибраного. Містить посилання на кошик товарів, список товарів, що сподобалися, і товари для порівняння."
                },
            boxHeaderPhone:
            {
                title:"Телефон",
                icon:<ContactPhoneIcon />,
                ariaLabel:"Розгорнути меню контактів",
                phoneHeaderPhone: [
                    { href: "tel:+380934915045", icon: <LifeCell />, text: "+380 (93) 491 50 45", label:"LifeCell" },
                    { href: "tel:+380668117899", icon: <Vodafone />, text: "+380 (50) 811 78 99", label:"Vodafone"}
                ],
                messengerHeaderPhone: [
                    { href: "https://telegram.im/diplomuskievua",   icon: <Telegram />, label:"Telegram", target: '_blank'  },
                    { href: "viber://chat?number=380668117899",     icon: <Viber />,    label:"Viber" },
                    { href: "whatsapp://send?phone=+380668117899",  icon: <WhatsApp />, label:"WhatsApp"}
                ],
            },
            }
        }
 // Выбор конфигурации на основе текущего языка
 const currentConfig = isLang ? config.ua : config.ru;

 // Путь к свойству разбивается на части
 const propsPath = prop.split(".");

 // Доступ к свойству через цепочку вызовов
 let result = currentConfig;
 for (const p of propsPath) {
     result = result[p];
     if (!result) break; // Если какое-то свойство не найдено, прерываем цикл
 }

 return result||[];
};
export default getConfigMenu;
